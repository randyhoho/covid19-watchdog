# c16-final-project-04-tw

## Name
Covid Watchdog

## Description
Our Team built an application regarding the information of  COVID-19 cases in Hong Kong such as providing users with distribution of any possibly infectious places where potential Covid-19 patients may have been visited. Users can hence be reminded how risky they are if they must go to their areas of interest.  The application will be automatically updated instantly with the latest information provided by the Centre for Health Protection of the Department of Health. More Add-on features are being adding to the App.

***

## Authors and acknowledgment
| | Name|
| :---: | :---: |
Mentor | Gordon Lau
Team Member(names not listed in order)| Louis Lo,Randy Ho, John Pang, Waylon Yu


## Installation
```
git clone https://gitlab.com/randyhoho/covid19-watchdog.git
and then follow the instruction of 'Setting up the development environment' as follow:
https://reactnative.dev/docs/environment-setup
```

## Key Features:  
1. Risky location identification 
2. Interactive Map 
3. Instant News Update 
4. Symptoms & Diagnosis 
5. Notification reminder 

## Timeline
| Date | Timeline |
| :---: | :---: |
2021-09-28 |DAE/FRD Day 1
2021-09-29 | Briefing for Final Project (TW/SW: 15:00 - 15:30, Venue: TW 5/F Campus and Zoom)
2021-10-15	|Final project period Day 1
2021-10-15	|Full name to CMS (Deadline)
2021-10-15	|CV Photo Shooting (TW: 10:45 - 13:00, SW: 13:20-16:30)
2021-10-22	| 4days to deploy iOS 、 7days to deploy Play Store
2021-10-29	|Career Workshop & CV Workshop (TW: 11:00 - 12:30, SW: 15:00-16:30)
2021-11-03	|Developer Showcase Rehearsal (TW/SW: 10:00 - 16:30)
2021-11-04	|Developer Showcase XIII (TW/SW: 15:00 - 19:00)


## Project status
Rapidly developing

## Support

Feel free to contact our team(names not listed in order):
| Name | e-mail|
| :---: | :---: |
John Pang| pang19941020@gmail.com
Randy Ho | randyhoho2020@gmail.com
Waylon Yu|waylon938@gmail.com
Louis Lo|louislo1995102@gmail.com

## temporary guideline for server setup:


- **I. setup postgres if migration is not ready**
1. run backend/sql/step1_dropAllTable.sql in vscode
2. run step 2 in vscode
3. run step 3 in vscode

- **II_setup localhost**
1. go to backend/main.ts
2. in terminal type: 
> `ts-node main.ts`




## Waylon's initial guideline:

https://reactnavigation.org/docs/getting-started/
```
npm install @react-navigation/native
npm install react-native-screens react-native-safe-area-context
npm install @react-navigation/native-stack
npm install @react-navigation/bottom-tabs
```

https://www.npmjs.com/package/react-native-vector-icons
```
npm install react-native-vector-icons
npm install @types/react-native-vector-icons
```

## installation guideline for Mac Users
running environment: Big Sur 11.5.2 | XcodeVersion 12.5.1 (12E507) | CPU: intel i5 | date: 2021 Oct 9 | Author: Randy Ho

step 1

> create a clean directory, make sure there is no .git on top of the folder.

step 2

```console
git clone git@gitlab.com:randyhoho/c16-final-project-04-tw.git
```

step 3
```console
npm install
```
step 4
```console
sudo gem install cocoapods
```
step 5

> open folder at directory/iOs/, double click Virgin.xcworkspace
john(cd ios -> pod install)

step 6
> if encounter error like: Unable to load contents of file list: 'xxxxx/Pods/Target Support Files/Pods-xxxx/Pods-xxxxx-frameworks-Debug-input-files.xcfilelist' (in target 'xxxxx')
step 6.1 
> completely close xcode
step 6.2 
```console
sudo gem update cocoapods --pre
```
step 6.3
```console
pod update
```
step 7 
> run Virgin.xcworkspace again and choose your phone, it should work.

**Q & A:**

1. 
> Multiple commands produce' error when building with new Xcode build system

Adding this line to the Podfile (under iOS directory)
```console
install! 'cocoapods', :disable_input_output_paths => true
```
then run  
```console
pod install
```
https://stackoverflow.com/questions/50718018/xcode-10-error-multiple-commands-produce/56964761


2. 
> Google Map Setting
> [java & objective-C ](https://github.com/react-native-maps/react-native-maps/blob/HEAD/docs/installation.md#enabling-google-maps)
`geolib`
`react-native-geolocation-service`
`react-native-maps`
