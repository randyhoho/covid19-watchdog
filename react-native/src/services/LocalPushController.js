import PushNotification from 'react-native-push-notification'

PushNotification.configure({
  // (required) Called when a remote or local notification is opened or received
  onNotification: function (notification) {
    console.log('LOCAL NOTIFICATION ==>', notification)
  },

  popInitialNotification: true,
  requestPermissions: true
})

export const LocalNotification = () => {
  PushNotification.localNotification({
    channelId: "111",
    autoCancel: true,
    bigText: '',
    subText: '',
    title: '警告！您正處於近期內曾有確診者經過地方的400米範圍內!',
    message: '請帶好口罩並保持社交距離',
    vibrate: true,
    vibration: 300,
    playSound: true,
    soundName: 'default',
    // actions: '["Yes", "No"]'
    largeIcon: "app_icon", // (optional) default: "ic_launcher". Use "" for no large icon.
    largeIconUrl: "../android/app/src/main/res/mipmap-hdpi/app_icon.png", // (optional) default: undefined
    smallIcon: "app_icon", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
    smallIconUrl: "../android/app/src/main/res/mipmap-hdpi/app_icon.png",
    bigLargeIcon: "app_icon", // (optional) default: undefined
    bigLargeIconUrl: "../android/app/src/main/res/mipmap-hdpi/app_icon.png", // (optional) default: undefined
  })
}