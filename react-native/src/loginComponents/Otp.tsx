import React from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
    StyleSheet
} from 'react-native';

export default function Otp(props: {
    code: any
    setCode: any
    confirmCode: any
    phoneNum: any
}) {
    return (
        <View style={styles.modalView}>
            <Text style={styles.modalTitle}>{props.phoneNum}</Text>
            <TextInput value={props.code} onChangeText={text => props.setCode(text)} style={styles.input} keyboardType="numeric" autoFocus />
            <Button title="Confirm Code" onPress={() => props.confirmCode()} />
        </View>
    )
}

const styles = StyleSheet.create({
    modalView: {
        width: '100%',
        height: '75%',
        backgroundColor: "grey",
        padding: 35,
        alignItems: "center",
        elevation: 5,
        alignContent: 'center'
    },
    modalTitle: {
        fontSize: 30,
        marginBottom: 10,
    },
    input: {
        backgroundColor: 'white',
        width: 300,
        height: 50,
        fontSize: 20,
        marginTop: 10,
        borderRadius: 10,
        paddingLeft: 10,
    },
})