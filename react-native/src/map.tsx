import React, { useEffect, useState, useRef } from 'react';
import {
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Linking,
  Button
} from 'react-native';
import { REACT_APP_BACKEND_URL } from '@env';

//? location related imports:
import { HA樣本包收發點 } from './mapComponent/convertor/output/latlngHA樣本包收發點';
import { getCoordinateKey, getDistance } from 'geolib';
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';

//? decoration:
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { check, checkMultiple, PERMISSIONS, requestMultiple, request, RESULTS } from 'react-native-permissions';

import PushNotification, { Importance, PushNotificationObject } from 'react-native-push-notification';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { LocalNotification } from './services/LocalPushController'

PushNotification.createChannel(
  {
    channelId: "111", // (required)
    channelName: "My channel", // (required)
    channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
    playSound: false, // (optional) default: true
    soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
  },
  (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
);

PushNotification.configure({
  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);

    // process the notification

    // (required) Called when a remote is received or opened, or local notification is opened
  },

  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);

  },

  onRegistrationError: function (err) {
    console.error(err.message, err);
  },

  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  popInitialNotification: true,


  requestPermissions: true,
});



const { width, height } = Dimensions.get('window');
const CARD_HEIGHT = 0;
const CARD_WIDTH = width * 0.8;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

if (Platform.OS === 'android') {
  checkMultiple([PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION])
    .then((result: any) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          console.log(
            'This feature is not available (on this device / in this context)');
          break;
        case RESULTS.DENIED:
          console.log(
            'The permission has not been requested / is denied but requestable');
          break;
        case RESULTS.LIMITED:
          console.log('The permission is limited: some actions are possible');
          break;
        case RESULTS.GRANTED:
          console.log('The permission is granted');
          break;
        case RESULTS.BLOCKED:
          console.log('The permission is denied and not requestable anymore');
          break;
      }
    })
    .catch((error: any) => {
      console.log(error);
    });
}

if (Platform.OS === 'android') {
  requestMultiple([PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION, PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION])
    .then(async (result) => {
      console.log(
        'Fine Location permission', await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION),
        'Background Location permission', await check(PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION)
      );
    })
    .catch((error: any) => {
      console.log(error);
    });
}
if (Platform.OS === 'ios') {
  request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
    .then(async result => {
      console.log(
        'Location permission',
        await check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE),
      );
    })
    .catch((error: any) => {
      console.log(error);
    });
}

PushNotification.popInitialNotification((notification) => {
  console.log('Initial Notification', notification);
});



PushNotification.createChannel(
  {
    channelId: "channel-id",
    channelName: "My channel",
    channelDescription: "A channel to categorise your notifications",
    playSound: false,
    soundName: "default",
    importance: Importance.HIGH,
    vibrate: true,
  },
  (created) => console.log(`createChannel returned '${created}'`)
);

PushNotification.configure({
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);


    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);


  },


  onRegistrationError: function (err) {
    console.error(err.message, err);
  },


  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },


  popInitialNotification: true,


  requestPermissions: true,
});

interface ILocation {
  latitude: number;
  longitude: number;
}

const Map = () => {
  const [dataset, setDataset] = useState(HA樣本包收發點);
  const [userLocation, setUserLocation] = useState<ILocation | undefined>(undefined)

  // useEffect(() => {
  //   Geolocation.requestAuthorization('whenInUse');
    // Geolocation.getCurrentPosition(
    //   position => {
    //     const { latitude, longitude } = position.coords;
    //     setUserLocation({
    //       latitude,
    //       longitude,
    //     });
    //   },
    //   error => {
    //     console.log(error.code, error.message);
    //   },
    //   { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
    // );
  // }, [])

  const get14days = async () => {
    try {
      let res = await fetch(`https://covid19.watchdog.health/past14days`)
      let json: any = await res.json();
      const past14days: any = json.data;
      setDataset(past14days);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    get14days();
  }, []);

  useEffect(() => {
    setInterval(function () {
     
      Geolocation.getCurrentPosition(
        urPosition => {
          const urlatitude = urPosition.coords.latitude;
          const urlongitude = urPosition.coords.longitude;
          const multipleDistance = dataset.map((location, i) => {
            const latitudeB = location.latitude;
            const longitudeB = location.longitude;

            const multipleDistance = getDistance(
              { latitude: urlatitude, longitude: urlongitude },
              { latitude: latitudeB, longitude: longitudeB },
            );
            if (multipleDistance < 400) {
              LocalNotification();
              console.log('因住,太近了')
            }
          });
          return multipleDistance;
        },
        error => {
          console.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      )
    }, 5*60*1000)
  }, [])

  const initialMapState = {
    dataset,
    categories: [
      {
        name: '過去14天內',
        icon: (
          <Ionicons name="ios-restaurant" style={styles.chipsIcon} size={18} />
        ),
      },
      {
        name: '社區檢測中心',
        icon: (
          <MaterialCommunityIcons
            style={styles.chipsIcon}
            name="food-fork-drink"
            size={18}
          />
        ),
      },
      {
        name: '強檢指明地方599J',
        icon: (
          <Ionicons name="ios-restaurant" style={styles.chipsIcon} size={18} />
        ),
      },
      {
        name: 'HA樣本包收發點',
        icon: <Fontisto name="hotel" style={styles.chipsIcon} size={15} />,
      },
    ],
    region: {
      latitude: 22.62938671242907,
      longitude: 88.4354486029795,
      latitudeDelta: 0.04864195044303443,
      longitudeDelta: 0.040142817690068,
    },
  };

  const [state, setState] = useState(initialMapState);
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  let mapRef: any = useRef(null)
  function moveToUser() {
    if (userLocation == undefined) {
      Geolocation.getCurrentPosition(
        position => {
          const { latitude, longitude } = position.coords;
          setUserLocation({
            latitude,
            longitude,
          });

          let location = { latitude:latitude, longitude: longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA }
          mapRef.current.animateToRegion(location, 500)
        },
        error => {
          console.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      );
    }else{
      let location = { latitude:userLocation.latitude, longitude: userLocation.longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA }
      mapRef.current.animateToRegion(location, 500)
    }
  }

  return (
    <SafeAreaView style={backgroundStyle}>
      <View style={styles.banner}>
        <Text style={styles.bannerText}>近期確診者經過的位置</Text>
        <TouchableOpacity onPress={moveToUser} style={styles.userLocationButton}><Text style={styles.userLocationText}>現在位置</Text></TouchableOpacity>
      </View>
      <MapView
        ref={mapRef}
        showsUserLocation={true}
        followsUserLocation={true}
        showsMyLocationButton={false}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 22.302711,
          longitude: 114.177216,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        }
        }
      >
        {dataset.map((marker, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            title="test"
            description="testing description">
            <Callout tooltip={true}>
              <View>

                <Text style={styles.cardtitle}>{marker.buildingname}</Text>

                <Text
                  style={styles.cardtitle}

                >
                  相關疑似/確診個案編號: {marker?.casenum}
                </Text>
              </View>
            </Callout>
          </Marker>
        ))}
      </MapView>

    </SafeAreaView>
  );
};

export default Map;

const styles = StyleSheet.create({
  markerImg: { width: 100, height: 100 },
  map: { height: '100%' },
  bannerText: {
    color: 'black',
    fontSize: 30,
    textShadowColor: 'black',
    textShadowOffset: { width: 2, height: 2 }
  },
  container: {
    flex: 1,
  },
  banner: {
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInput: { flex: 1, padding: 0 },
  datasetMarker: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 107 : 20,
    flexDirection: 'row',
    // backgroundColor: '#000',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    // paddingHorizontal: 0,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#7FFFD4',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 30,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    // padding: 10,
    elevation: 2,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { width: 2, height: -2 },
    // height: CARD_HEIGHT,
    // width: CARD_WIDTH,
    overflow: 'hidden',
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  textContent: {
    flex: 2,
    padding: 10,
  },
  cardtitle: {
    fontSize: 20,
    // marginTop: 5,
    backgroundColor: '#FFF',
    fontWeight: 'bold',
  },
  cardDescription: {
    fontSize: 12,
    color: '#444',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
  },
  marker: {
    width: 30,
    height: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  userLocationButton: {
    height: 30,
    width: 100,
    borderWidth: 2,
    borderRadius: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#5DB075',
    borderColor: '#5DB075',
    marginBottom: 5,
  },
  userLocationText: {
    color: 'white'
  }
});
