import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Modal,
    FlatList,
    SafeAreaView,
    Alert,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PhoneInput from 'react-native-phone-number-input';
import auth, { firebase } from '@react-native-firebase/auth';
import Login from './loginComponents/Login';
import Otp from './loginComponents/Otp';
import Register from './loginComponents/Register';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux'
import { login } from './redux/auth/thunk';

const DATA = [
    {
        id: "1",
        title: "更改個人資料",
        icon: <Ionicons name="settings-outline" size={25} color="white" />,
    },
    {
        id: "2",
        title: "助養本軟件",
        icon: <MaterialCommunityIcons name="charity" size={25} color="white" />,
    },
    {
        id: "3",
        title: "回報問題",
        icon: <FontAwesome name="exclamation-circle" size={25} color="white" />,
        link: 'ReportProblem'
    },
];

export default function Settings() {
    const [modalVisible, setModalVisible] = useState(false);
    const [loginVisible, setLoginVisible] = useState(true);
    const [otpVisible, setOtpVisible] = useState(false);
    const [registerVisible, setRegisterVisible] = useState(false);

    const [selectedId, setSelectedId] = useState(null);

    // phone auth
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const [confirm, setConfirm]: any = useState(null);
    const [code, setCode] = useState('');

    // phone input
    const [phoneValue, setPhoneValue] = useState('');
    const [formattedValue, setFormattedValue] = useState('');
    const [valid, setValid] = useState(false);
    const phoneInput = useRef<PhoneInput>(null);

    const navigation = useNavigation();
    const dispatch = useDispatch();

    async function validateNumber() {
        const checkValid = phoneInput.current?.isValidNumber(phoneValue);
        setValid(checkValid ? true : false);
        if (checkValid === true) {
            await signInWithPhoneNumber(formattedValue);
            setLoginVisible(!loginVisible);
            setOtpVisible(!otpVisible);
        } else {
            Alert.alert('invalid format!');
        }
    }

    async function signInWithPhoneNumber(phoneNumber: any) {
        const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
        setConfirm(confirmation);
    }

    async function confirmCode() {
        try {
            await confirm.confirm(code);
            let userToken = await auth().currentUser!.getIdToken()
            dispatch(login(userToken))
            // setModalVisible(false);
        } catch (error) {
            Alert.alert('invalid code');
        }
    }

    async function onAuthStateChanged(user: any) {
        setUser(user);
        if (initializing) setInitializing(false)
    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
        return subscriber
    })

    const Item = ({ item }: any) => (
        <Pressable style={[styles.settingTabs]} onPress={() => navigation.navigate(item.link ? item.link : item.title)}>
            <Text style={[styles.tabText]}>{item.icon} {item.title}</Text>
        </Pressable>
    );

    const renderItem = ({ item }: any) => {
        return (
            <Item
                item={item}
                onPress={() => setSelectedId(item.id)}
            />
        );
    };

    if (initializing) return null;

    return (
        <View style={[styles.container, { flexDirection: 'column' }]}>
            <Modal
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <Pressable style={styles.closeModal} onPress={() => setModalVisible(!modalVisible)} />
                    {loginVisible && <Login phoneInput={phoneInput} value={phoneValue} setPhoneValue={setPhoneValue} setFormattedValue={setFormattedValue} validateNumber={validateNumber} />}
                    {otpVisible && <Otp code={code} setCode={setCode} confirmCode={confirmCode} phoneNum={phoneValue} />}
                    {registerVisible && <Register />}
                </View>
            </Modal>


            <Image style={[styles.backgroundImg, styles.topImg]} source={require('./assets/settingsBackground.png')} />
            <Image style={[styles.backgroundImg, styles.botImg]} source={require('./assets/settingsBackground.png')} />

            <View style={[styles.topbar]}>
                <FontAwesome name="user-circle" size={100} color="black" />
                {firebase.auth().currentUser ? <View style={styles.userButton}><Text style={styles.buttonText}>{firebase.auth().currentUser!.phoneNumber}</Text></View> :
                    <Pressable style={[styles.userButton]} onPress={() => setModalVisible(true)}>
                        <Text style={[styles.buttonText]}>登入／註冊</Text>
                    </Pressable>}
            </View>

            <SafeAreaView style={styles.settingBody}>
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                    extraData={selectedId}
                />
            </SafeAreaView>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    backgroundImg: {
        width: 170,
        height: 170,
    },
    topImg: {
        position: 'absolute',
        top: 70,
        right: 10,
    },
    botImg: {
        position: 'absolute',
        bottom: 70,
        left: 10,
    },
    topbar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 30,
    },
    userButton: {
        height: 50,
        width: 200,
        borderWidth: 2,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5DB075',
        borderColor: '#5DB075',
    },
    buttonText: {
        fontSize: 25,
        color: 'white',
    },
    settingBody: {
        flex: 4,
        alignItems: 'center',
    },
    settingTabs: {
        height: 50,
        width: 350,
        backgroundColor: '#5DB075',
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
    },
    tabText: {
        fontSize: 25,
        color: 'white',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    closeModal: {
        width: '100%',
        height: '30%',
        backgroundColor: 'black',
        opacity: 0.7,
    },
});
