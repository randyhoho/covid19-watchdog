import React from 'react';
import {
    ScrollView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import { Controller, useForm } from 'react-hook-form';



export default function AccountSettings() {
    const { control, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = (data: any) => console.log(data)
    return (

        <ScrollView>
            <Text>更改個人資料</Text>
            <Controller control={control} rules={{ required: true, }} render={({ field: { onChange, onBlur, value } }) => (
                <TextInput style={styles.input} onBlur={onBlur} onChangeText={onChange} value={value} placeholder="請輸入您的名字" />
            )}
                name="name"
                defaultValue=""
            />
            {/* {errors.firstname && <Text style={styles.errorText}>請輸入名字</Text>} */}
            <Controller control={control} rules={{ required: true, }} render={({ field: { onChange, onBlur, value } }) => (
                <TextInput style={styles.input} onBlur={onBlur} onChangeText={onChange} value={value} placeholder="請輸入您的住址" />
            )}
                name="address"
                defaultValue=""
            />
            {/* {errors.address && <Text style={styles.errorText}>請輸入住址</Text>} */}
            <Controller control={control} rules={{ required: true, }} render={({ field: { onChange, onBlur, value } }) => (
                <TextInput keyboardType='numeric' style={styles.input} onBlur={onBlur} onChangeText={onChange} value={value} placeholder="請輸入您的年齡" />
            )}
                name="age"
                defaultValue=""
            />
            {/* {errors.age && <Text style={styles.errorText}>請輸入年齡</Text>} */}
            <TouchableOpacity style={styles.userButton} onPress={handleSubmit(onSubmit)}><Text>確定</Text></TouchableOpacity>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    input: {
        backgroundColor: 'white',
        width: 300,
        height: 50,
        fontSize: 20,
        marginTop: 10,
        borderRadius: 10,
        paddingLeft: 10,
    },
    userButton: {
        height: 50,
        width: 200,
        borderWidth: 2,
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5DB075',
        borderColor: '#5DB075',
    }
})