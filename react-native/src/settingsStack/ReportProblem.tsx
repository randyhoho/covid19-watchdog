import React, { useState } from 'react';
import { 
    FlatList, 
    SafeAreaView, 
    StyleSheet, 
    Text, 
    View,
    Pressable,
    ScrollView
} from "react-native";
import { useNavigation } from '@react-navigation/native'

const DATA = [
    {
        id: "1",
        title: "數據",
    },
    {
        id: "2",
        title: "新聞",
    },
    {
        id: "3",
        title: "地圖",
    },
    {
        id: "4",
        title: "診斷",
    },
    // {
    //     id: "5",
    //     title: "帳號",
    // },
    {
        id: "6",
        title: "其他",
    },
];

export default function ReportProblem() {
    const [selectedId, setSelectedId] = useState();
    const navigation:any = useNavigation()
    
    const Item = ({ item }: any) => (
        <Pressable style={styles.settingTabs} onPress={() => navigation.navigate('傳送問題回報', {title: item.title})}>
            <Text style={[styles.tabText]}>{item.title}</Text>
        </Pressable>
    );

    const renderItem = ({ item }: any) => {
        return (
            <Item
                item={item}
                onPress={() => setSelectedId(item.id)}
            />
        );
    };

    return (
        // <ScrollView>

        <View style={styles.container}>
            <View style={styles.banner}>
                <Text style={styles.title}>選擇問題範圍</Text>
            </View>
            <SafeAreaView>
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                    extraData={selectedId}
                />
            </SafeAreaView>
        </View>

        // </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    banner: {
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 30,
        color: 'black'
    },
    item: {
        padding: 20,
    },
    itemTitle: {
        fontSize: 25,
        color: 'black'
    },
    settingTabs: {
        height: 50,
        width: 350,
        backgroundColor: '#5DB075',
        borderRadius: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
    },
    tabText: {
        fontSize: 25,
        color: 'white',
    },
});
