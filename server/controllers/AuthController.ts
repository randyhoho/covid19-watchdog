import { UserService } from "../services/UserService";
import { Request, Response } from 'express';


import { initializeApp } from 'firebase-admin/app';
const admin = require('firebase-admin');
let serviceAccount = require("../firebase/covidappotp-d2775-firebase-adminsdk-9d50d-511b8f77b1.json")

initializeApp({
    credential: admin.credential.cert(serviceAccount),
});  

export class AuthController {
    public constructor(private userService: UserService) {

    }
    
    login = async (req: Request, res: Response ) => {
        try {
            const token = req.body.token
            let decodedToken = await admin.auth().verifyIdToken(token)
            const user = await this.userService.getUserByPhone(decodedToken.phone_number)
            return res.json({token: token, user: user})

        } catch (e) {
            console.log(e);
            return res.status(500).json({error: 'Unknown error'})
        }
    }
}