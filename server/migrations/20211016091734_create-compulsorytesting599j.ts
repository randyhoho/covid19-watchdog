import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable1 = await knex.schema.hasTable('compulsorytesting599j');
    if(!hasTable1){
        await knex.schema.createTable('compulsorytesting599j',(table)=>{
            table.increments();
            table.date("hadbeenfrom");
            table.date("hadbeento");
            table.date("deadline"); 
            table.date("passfrom");
            table.date("passto");
            table.text("address");
            table.specificType('latitude', 'double precision')
            table.specificType('longitude', 'double precision')
            table.timestamps(false,true);
        });  
    }
    const hasTable2 = await knex.schema.hasTable('covidplace');
    if(!hasTable2){
        await knex.schema.createTable('covidplace',(table)=>{
            table.increments();
            table.date("createat");
            table.date("visitdate");
            table.text("district");
            table.text("buildingname");
            table.text("casenum");
            table.specificType('latitude', 'double precision')
            table.specificType('longitude', 'double precision')
            table.timestamps(false,true);
        });  
    }

    const hasTable3 = await knex.schema.hasTable('news');
    if(!hasTable3){
        await knex.schema.createTable('news',(table)=>{
            table.increments();
            table.text("articles__source__name");
            table.text("articles__author");
            table.text("articles__title");
            table.text("articles__description");
            table.text("articles__url");
            table.text("articles__urlToImage");
            table.text("articles__publishedAt");
            table.text("articles__content");
            table.timestamps(false,true);
        });  
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('compulsorytesting599j');
    await knex.schema.dropTableIfExists('covidplace');
    await knex.schema.dropTableIfExists('news');

}

