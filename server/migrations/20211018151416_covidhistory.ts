import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('covidhistory');
    if(!hasTable){
        await knex.schema.createTable('covidhistory',(table)=>{
            table.increments();
            table.integer("casenum")
            table.text("reportdate");
            table.text("DISTRICT_EN");
            table.text("DISTRICT_TC");
            table.text("BLDGNAME_EN");
            table.text("BLDGNAME_TC");
            table.integer("EASTING");
            table.integer("NORTHING");
            table.specificType('lat', 'double precision')
            table.specificType('long', 'double precision')
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('covidhistory');
}

