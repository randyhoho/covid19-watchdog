import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if (!hasTable) {
        return await knex.schema.createTable('users',table=> {
            table.increments();
            table.string('name').notNullable();
            table.integer('age').notNullable();
            table.string('phone').notNullable();
            table.string('address').notNullable();
            table.timestamps(false, true);
        })
    } else {
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('users')
}

