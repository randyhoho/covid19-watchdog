-- 全table del:
DO $$ DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema()) LOOP
        EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
    END LOOP;
END $$;


-- SELECT * FROM users;
-- DROP TABLE users;

SELECT * FROm "problem-reports";
SELECT * FROM fourteendays;
DROP TABLE users;